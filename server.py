from bottle import route, run, template, static_file, request

@route('/')
def index():
	return template('index')

@route('/hello')
def hello():
	return "Hello World!"

@route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='/home/maya/Projects/maya')

@route('/tofesmatarot')
def tofesmatarot():
	return template('tofesmatarot')
    
@route('/tofesmatarot', method="POST")
def show_matara():
	matara = request.forms.mymatara

	matarot = ["בוגר הרואה את האדם ואושרו כיסוד מרכזי בהוויה ומקיים אורחות חיים המבטאים אמונה זו.",
			"בוגר בעל עמדות דמוקרטיות וסוציאליסטיות.",
			"בוגר הרואה את מסורת ישראל כמקור השראה ולא כמקור סמכות.",
			"בוגר שהאחריות האישית והטלת הספק הינם יסודות בגיבוש תפיסת עולמו ומימושה.",
			"בוגר המבסס את קיומו החומרי והתרבותי על עשייה אישית.",
			"בוגר המעורב ופעיל בתהליכי שינוי חברתיים בכיוון אמונותיו ותפיסותיו.",
			"בוגר השואף להגשים עצמו במדינת ישראל ושותף לתהליכי בנייתה והגנתה.",
			"בוגר השואף להגשים עצמו במסגרת הקיבוץ או במסגרות שיתופיות סוציאליסטיות אחרות."]

	mymatara = matarot[int(matara)-1]

	return template('matarot', matara=mymatara)

@route('/tofes')
def tofes():
	return template('tofes')
    
@route('/tofes2')
def show_future():
	friend = request.query.mybf
	grandma = request.query.mygrandma
	bus = request.query.mybus
	animal = request.query.myanimal
	curse = request.query.mycurse
	return template('future', friend=friend, grandma=grandma, bus=bus, animal=animal, curse=curse)

run(host='localhost', port=8080, debug=True)